#!/usr/bin/env bash

# Note that there are a lot of 'if' statements, that purely protect your environment from re-configurations.
#    I can not find a clearly documented process on 'simple' reconfigurations of environments, nor can I find
#    just a single place to complete said task. (Homestead, Vagrant, ugh!)

# Default apt-get update, to ensure that there are no untrusted packages.
sudo apt-get update
# Install PHP Environment (Default includes Apache2)
if ! [ -f /usr/bin/php ]; then
  sudo apt-get install -y php5
fi

# Download composer, and move to appropraite directory
if ! [ -f /usr/local/bin/composer ]; then
  sudo curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin -- --filename=composer
fi
# Now redundant, as it's installed immediately to the correct location - sudo mv /home/vagrant/composer.phar /usr/local/bin/composer

# Install MySQL Environment (I prefer this over SQLITE3, but each to their own!)
#    These debconf-set-selections force some default settings, which would otherwise require user input.
#    I highly recommend you update / change these.
if ! [ -f /usr/sbin/mysqld ]; then
  sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password default'
  sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password default'
  sudo apt-get -y install mysql-server
  sudo apt-get -y install php5-mysqlnd
fi

# SQLite and it's dependancies relative to Laravel
if ! [ -f /usr/bin/sqlite3 ]; then
  sudo apt-get -y install sqlite3
  sudo apt-get -y install php5-sqlite
fi

# Prepare my oAuth keys for GitHub.
#    The laravel project files require a large amount of API calls, which requires a legit account.
if ! [ -f /root/.composer/auth.json ]; then
  sudo mkdir /root/.composer/
  sudo ln -s /vagrant/auth.json /root/.composer/
fi

# Use composer to download the laravel installer, and symlink the file
# This may no longer be required; composer global require "laravel/installer=~1.1"
#    The only downside of the above removal, is the project files will be downloaded each provision.
if ! [ -d "/var/www/laravel/public" ]; then
  sudo composer create-project laravel/laravel /var/www/laravel --prefer-dist
fi

# Copy our 000-default.conf file to the server
sudo cp /vagrant/000-default.conf /etc/apache2/sites-available/

# Enable the rewrite mod of apache, which removes the requirement of having index.php in pages.
# The second half of this configuration is in the 000-default.conf file - AllowOverride All
#    http://www.domain.tld/index.php/contact
#      vs
#    http://www.domain.tld/contact
sudo a2enmod rewrite

# Now everything is setup, lets restart apache.
sudo /etc/init.d/apache2 restart
