# README #

Important: virtualbox-guest-utils has been moved to contrib package tree.

Symptom:

```
#!bash

vagrant up
```

    default: No guest additions were detected on the base box for this VM! Guest
    default: additions are required for forwarded ports, shared folders, host only
    default: networking, and more. If SSH fails on this machine, please install
    default: the guest additions and repackage the box to continue.
    default: 
    default: This is not an error message; everything may continue to work properly,
    default: in which case you may ignore this message.

    Failed to mount folders in Linux guest. This is usually because
    the "vboxsf" file system is not available. Please verify that
    the guest additions are properly installed in the guest and
    can work properly. The command attempted was:

    mount -t vboxsf -o uid=`id -u www-data`,gid=`getent group www-data | cut -d: -f3`,dmode=775,fmode=664 laravel /var/www/laravel
    mount -t vboxsf -o uid=`id -u www-data`,gid=`id -g www-data`,dmode=775,fmode=664 laravel /var/www/laravel

    The error output from the last command was:

    stdin: is not a tty
    mount: unknown filesystem type 'vboxsf'


After a 'medium' delay, results in timeout error that vboxsf is an unknown filesystem.

Resolution:

https://wiki.debian.org/VirtualBox#Debian_8_.22Jessie.22

Effectively; VirtualBox has been moved to Contrib, and these repositories are then required to support Shared Folders.


```
#!bash

sudo apt-get update
sudo apt-get install virtualbox-guest-utils
```

Restart your vagrant box, and GET CODING WITH LARAVEL!

IMPORTANT:

During provisioning; Downloading Composer can sometimes fail due API rate limits, and usage of OAuth Tokens - reasons outside scope of this project. To avoid this, please follow the steps located here;

https://getcomposer.org/doc/articles/troubleshooting.md#api-rate-limit-and-oauth-tokens

Store the auth.json file in the same root directory as the rest of this project.

Description:

In my experiences attempting to start developing with the Laravel Framework, I found the initial configuration extremely convoluted. There appeared to be no easy way to just 'get in there' with the Framework.

So once I got the environment provisioning quickly with Vagrant, I created the appropriate bootstrap.sh and Vagrant files to share for others!

I hope this helps you in your initial journey with Laravel!

### What is this repository for? ###

* Quick Summary:

This repository is aimed at users trying to quickly establish a Laravel development environment within Vagrant, without having to jump through 1 million hoops.
* Version:

1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

Example Folder Structure

/home/[user]/Development/[Project Name]/[Laravel]

Example:

/home/iDusty/Development/ExampleProject/Laravel

Where; ExampleProject is my Vagrant root folder, and Laravel is the folder our Laravel Framework will be created in (created in step 2 below).

* Summary of set up:

Step 1: Pull these files into your [Project Name] folder listed above (Your Vagrant folder)]

Step 2: Create a folder named 'Laravel'.

Step 3: Start your vagrant machine 'vagrant up --provision'.

Step 4: Wait for Vagrant to import the Debian VagrantCloud box, then download ALL required prerequisites for Laravel

Step 5: Start working with the Laravel framework. (within the Laravel folder)

* Configuration:

There is no configuration necessary at this stage, however I do not recommend using the project defaults in a production environment - as the passwords are not complex.

* Dependencies

Vagrant

* Database configuration

Username(s) and password(s) can be found in bootstrap.sh

* How to run tests

Navigate to http://localhost:8080/
If not working, ensure that box has finished provisioning, or that no other boxes were running at the time of provisioning, which will impact port redirection.

* Deployment instructions

As above.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin

Feel free to contact me, iDusty, if you have any comments or questions.

* Other community or team contact